#!/bin/bash
###################################################################
# Instalador automatizado do keepalived Master e Slave
# 1.0v
# Autor: Guilherme Martins
# Criado em 12/11/2019
###################################################################

IP=`hostname -I | awk '{print $1}'`
logfile='/var/log/keepalived-installer.log'
dirlog='/var/log'
CONFIGFILE='/etc/keepalived/keepalived.conf'

MENSAGEM_USO="
Uso: $(basename "$0") [OPÇÕES]

OPÇÕES:
	-h, --help 	Mostra esta tela de ajuda e sai
	-m, --master  	Instala keepalived como MASTER no servidor
	-s, --slave 	Instala keepalived como SLAVE (BACKUP) no servidor
"

function LOG(){
	echo "[`date \"+%d-%m-%Y %H:%M:%S:%s\"`] [Keepalived Installer] - $1" >> $logfile
}

function is_root_user() {
	if [[ $EUID != 0 ]]; then
    	return 1
  	fi
  	return 0
}

function installPkgs(){
	if [ -e $dirlog ]; then
	        LOG "Diretorio de backup ja existe"
	else
	        LOG "Criando diretório de backup"
	        mkdir -p $dirlog
	fi

	LOG "Atualizando pacotes necessarios"
	apt-get update ; apt-get -y upgrade ; apt-get -y install net-tools

}

#function adjustInterface(){
	#Será implementado esta função
#}

function installMaster(){

	LOG "Instalando keepalived"
	apt-get -y install keepalived

	if [[ $? -eq 0 ]]; then
		LOG "keepalived instado com sucesso, configurando..."

		echo "Informe IP Virtual a ser utilizado" ; read VIRTUALIP
		echo "Informe o nome da Interface de rede do servidor SLAVE" ; read MASTERINTERFACE
		echo "Informe a senha a ser utilizada entre os servidores: " ; read PASSWDSRV

		if [[ -e $CONFIGFILE ]]; then
			mv $CONFIGFILE{,.bak}
			echo "Arquivo de configuração ja existe, movendo para backup"
		fi
			echo -e "vrrp_script chk_myscript {
        	script       \"/usr/local/bin/mycheckscript.sh\" #script return: 0 false - 1 true
        	interval 2   # check every 2 seconds
        	fall 2       # require 2 failures for KO
        	rise 2       # require 2 successes for OK
			}

		vrrp_instance VI_1 {
	        interface $MASTERINTERFACE
	        state MASTER
	        virtual_router_id 51
	        priority 110
	        virtual_ipaddress {
	            $VIRTUALIP
	       	}
		authentication {
        	auth_type AH
        	auth_pass $PASSWDSRV
    	}
    	notify /usr/local/bin/keepalivednotify.sh
    	}" >> $CONFIGFILE
			
		LOG "Reiniciado keepalived"
		/etc/init.d/keepalived restart

		LOG "Habilitando serviço no boot do servidor"
		systemctl enable keepalived.service
	else
		LOG "Erro na instalação, verifique"
	fi
}

function installSlave(){

	LOG "Instalando keepalived Slave"
	apt-get -y install keepalived

	if [[ $? -eq 0 ]]; then
		LOG "keepalived instado com sucesso, configurando..."
		
		echo "Informe IP Virtual a ser utilizado" ; read VIRTUALIP
		echo "Informe o nome da Interface de rede do servidor SLAVE" ; read SLAVEINTERFACE
		echo "Informe a senha a ser utilizada entre os servidores: " ; read PASSWDSRV

		if [[ -e $CONFIGFILE ]]; then
			mv $CONFIGFILE{,.bak}
			echo "Arquivo de configuração ja existe, movendo para backup"
		fi
			echo -e "vrrp_script chk_myscript {
        	script       \"/usr/local/bin/mycheckscript.sh\" #script return: 0 false - 1 true
        	interval 2   # check every 2 seconds
        	fall 2       # require 2 failures for KO
        	rise 2       # require 2 successes for OK
			}

		vrrp_instance VI_1 {
	        interface $SLAVEINTERFACE
	        state BACKUP
	        virtual_router_id 51
	        priority 100
	        virtual_ipaddress {
	        $VIRTUALIP
	        }
		authentication {
        	auth_type AH
        	auth_pass $PASSWDSRV
    	 }
    	 notify /usr/local/bin/keepalivednotify.sh
    	 }" >> $CONFIGFILE
		
		LOG "Reiniciado keepalived"
		/etc/init.d/keepalived restart
		
		LOG "Habilitando serviço no boot do servidor"
		systemctl enable keepalived.service
	else
		LOG "Erro na instalação do keepalived, verifique!"
	fi
}

function MyCheckScript(){
	local MCSCRP='/usr/local/bin/mycheckscript.sh'
	    
	LOG "Criando mycheckscript"
    echo -e "#!/bin/bash

	ENDSTATE=\$3
	#NAME=\$2
	#TYPE=\$1

	case \$ENDSTATE in
	    \"BACKUP\") # ação quando o servidor entrar no estado BACKUP (Slave)
	              exit 0
	              ;;
	    #\"FAULT\")  # ação quando o servidor entrar no estado FAULT
	    #          exit 0
	    #          ;;
	    \"MASTER\") # ação quando o servidor entrar no estado MASTER
	              exit 0
	              ;;
	    *)        echo \"Estado desconhecido \${ENDSTATE} para VRRP \${TYPE} \${NAME}\"
	              exit 1
	              ;;
		esac" >> $MCSCRP && chmod +x $MCSCRP
}

if ! is_root_user; then
	echo "Você precisa ser root para executar a instalação!" 2>&1
	echo  2>&1
	exit 1
fi

if [[ -z $1 ]]; then
    echo "$MENSAGEM_USO"
fi

while [[ -n "$1" ]]; do
	case "$1" in
		-h | --help) 	echo "$MENSAGEM_USO" && exit 0 ;;
		-m | --master)  installPkgs && installMaster && MyCheckScript && exit 0;;
		-s | --slave)	installPkgs && installSlave && MyCheckScript && exit 0;;
		*) echo "Opção Invalida, utilize -h ou --help para ajuda" && exit 1 ;;
	esac
	shift
done
